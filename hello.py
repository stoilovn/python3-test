#!/usr/bin/env python3.8

"""

Purpose: Say hello to a specific person.

Author:  Nikola STOILOV

"""


import os
import argparse

def cli():
    """Permet de utiliser le script hello.py with parameter passed as argument."""
    my_parser = argparse.ArgumentParser(description="Saluer une personne",
                            usage='%(prog)s [options]') #pragma: no cover
    my_parser.add_argument("-n", "--name", dest="name",
                            metavar='str', help="The name to greet", default='') #pragma: no cover
    args = my_parser.parse_args() #pragma: no cover
    print(hello(args.name)) #pragma: no cover


def hello(name):
    """
    Function that salutes a specific person.

    Args:
    - name : str
         The name of the person to salute.

    Output:
    Returns string to salute a person.
    """
    try:
        assert isinstance(name,str)
        if not name:
            name = os.environ.get("USERNAME")

    except (AssertionError, TypeError) as _:
        return "Please give valid string"

    return "Hello, Long-named folk!" if len(name) > 16 else "Hello, " + name + "!"

if __name__ == "__main__":
    # Appel obligatoire pour notre interface CLI.
    cli() #pragma: no cover
