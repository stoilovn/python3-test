#!/usr/bin/env python
import setuptools

setuptools.setup(name='hello',
    py_modules=['hello'],
    scripts=['hello'],
    version='0.1.0',
    python_requires='>=3.6',
    description="Basic program to salute a person whose name is given as an argument",
    author='Nikola Stoilov',
    author_email='nicola.stoilov@gmail.com',
    url='https://gitlab.com/stoilovn/python3-test',
    license='GPL-3.0',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
)
