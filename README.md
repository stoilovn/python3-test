# Usage

`hello [options]`

# Install

- From PyPi: TODO  
- From local: `pip install .`  


# 2. Interfaces CLI:

On a choisi Argparse comme interface CLI.

Avantages:

- On peut executer le script sans aucun interaction de l'utilisateur supplementaire.
- L'utilisateur peut specifier plusieurs arguments (commands) qui donne
des resultats a l'instant (Faut pas modifier le code pour sortir le resultat exact qui est voulu,
faut juste donner des arguments)
- CLI utilises moins de ressources (memory, cpu), au contraire de GUI par example.


Desavantages:


- Pour les taches complexes, le CLI peut etre utiliser pour automatiser
les choses.
- Faut etre precise et ne pas avoir des fautes (--flod au lieu de --flood) qui va generer des erreurs.
- Plus dure a memoriser les differents commands si le personne est un "debutant"




### Comment obtenez-vous le nom d'utilisateur lorsque le nom n'est pas donne ?

- On utilise la bibliothque standart OS, qui nous permet de obtenir le nom de l'utilisateur qui a execute le script.
Dans notre cas ca sera avec la commande : os.environ.get('USERNAME')

- J'utilise cette bibliotque parce que ca nous donne le resultat voulu sous Windows et Linux. Je n'utilise pas des variables d'environements parce que c'est pas "safe" car les variables peuvent etre changees et notre code peut planter.



# 3. Arguementez votre choix pour le shebang:

Ca concerne [PEP394](https://www.python.org/dev/peps/pep-0394/)

Ca nous permet de specifier laquelle python interpreter on veut utiliser pour executer notre script -
dans notre cas ca sera python3.8

Je me suis base sur les ressources du StackOverflow qui expliquent tres bien l'utilisation de shebang pour python:

Quant a la portabilite, on doit utiliser:

- `#!/usr/bin/env python3` 

[Stackoverflow-Thread](https://stackoverflow.com/questions/2429511/why-do-people-write-usr-bin-env-python-on-the-first-line-of-a-python-script)

[Stackoverflow: Usage of /usr/bin/env python3](https://stackoverflow.com/questions/6908143/should-i-put-shebang-in-python-scripts-and-what-form-should-it-take)

Stackoverflow est le google pour les developpeurs, on peut trouver facilement une solution de notre probleme ainsi qu'une explication.


# 4. On a creer une script hello.py qui renvoie la chaine de charactere.

Ainsi la fonction cli() qui gere l'interface CLI.

On confirme:

`./hello.py -n nick` renvoie bien Hello, Nick !  

`./hello.py -n nicknicknickinicknick` (20 chars) renvoie bien Hello, long-named folk !  

`./hello.py` renvoie bien Hello, charetoile !  


### Quelle est la maniere pythonique de gerer le fait que la personne est optionnele ?

On peut avoir une valeur par defaut d'une parametre d'une fonction en python - comme: `def test(name="")` .
Donc maintenant on peut importer notre module, et on a pas besoin de mettre "empty string", pour qu'on puisse avoir Hello + username. Voici l'example:
```
import hello
hello.hello() renvoie bien "Hello, charetoile!"
``` 


Avant on devait specifier le string vide comme hello.hello("").

### Quelle est maniere pythonique d'importer bout de code ?

En creeant notre if __name__="__main__" pour faire notre code scalable,
on peut importer du bout de code en faisant import 'nom-du-module'
et apres on peut appeller le bout de code en specifiant la fonction avec '.'
Donc on peut faire:

```
import hello

hello.hello("Nick")
```

qui nous renvoie bien `Hello, Nick !`


# 5. Utilisez Pylint:

En utilisant Pylint, j'ai eu une note de 7.27 parce que
je n'ai pas ecris des docstrings.

Apres avoir ecris les docstrings, la note a augmente a 8.89.

Pour avoir notre code "compliant" a PEP8, j'utilise le package [autopep8](https://pypi.org/project/autopep8/). 
Je respecte presque toutes les recommendations, sauf le "avoir moins de 100 chars par ligne" car ca coupe la visibilite du code au return statement a la ligne 18 et 19.

# 6. Documentez script grace a Pylint, a la Pep 257.

J'ai bien documente mon script, de plus pour verifier qu'il est compatible avec Pep257 en utilisant
le pip package [pep257](https://pypi.org/project/pep257/)

En utilisant: `pep257 hello.py` on voit bien qu'il n y a pas des erreurs affichees.

# 7. Added setup.py and symbolic link hello => hello.py 

Maintenant on peut tester avec virtualenv:
```
virtualenv test
source test/bin/activate
pip3 install .
```

3 cases a confirmer:

- `hello -n nick` renvoie bien Hello, Nick !  

- `hello -n nicknicknickinicknick` renvoie bien Hello, long-named folk !  

- `hello` renvoie bien Hello, charetoile !  



# 9. Besoin de packages supplementaires ?

 Oui on doit installer le plugin pytest-cov avec pip3 install pytest-cov

### Quelle commande lancez-vous ?

Etant dans la repertoire ou le script se trouve, on lance la commande:

`pytest --cov=hello.py .`  

Pour generer une rapport HTML:

`pytest --cov-report html --cov=hello .`

Et apres on peut trouver notre rapport dans `htmlcov/index.html`