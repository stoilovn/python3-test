import hello

def test_answer():
	assert hello.hello("") == "Hello, charetoile!"
	assert hello.hello("Nick") == "Hello, Nick!"
	assert hello.hello("12345678912345678") == "Hello, Long-named folk!"
	assert hello.hello("Nikola") == "Hello, Nikola!"
	assert hello.hello(75) == "Please give valid string"
